<?php


namespace Scigeniq\Core\Controllers\ResourceControllers;


use Scigeniq\Core\Entity\EntityRepo;
use Scigeniq\Core\Entity\EntityRepoInterface;

class DefaultEntityRepo extends EntityRepo implements EntityRepoInterface
{

}
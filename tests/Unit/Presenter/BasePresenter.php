<?php


namespace Tests\Unit\Presenter;


use Scigeniq\Core\Presenter\Presenter;

class BasePresenter extends Presenter
{
    public function name()
    {
        return 'base';
    }
}